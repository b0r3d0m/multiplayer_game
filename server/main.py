import copy
import json
import socket
import random
import time

import jsonpickle


IP_ADDR = '0.0.0.0'
PORT = 12345


sock = socket.socket(socket.AF_INET,
                     socket.SOCK_DGRAM)
sock.bind((IP_ADDR, PORT))
sock.setblocking(0)


class Client:
  def __init__(self):
    self.world_state = {}
    self.seq_num = 0
    self.diffs = {}  # sent diffs (to apply it to the player state later). key: id, value: list of updated objects

  def touch(self):
    self.last_msg_time = time.time()

clients = {}  # key: addr, value: Client


class Player:
  def __init__(self, id, name, x, y, addr):
    self.id = id
    self.name = name
    self.x = x
    self.y = y
    self.addr = addr
    self.keys_states = {
      'up': False,
      'down': False,
      'left': False,
      'right': False
    }

  def __eq__(self, other): 
    return self.__dict__ == other.__dict__

  def __ne__(self, other): 
    return self.__dict__ != other.__dict__


class World:
  def __init__(self, w, h):
    self.w = w
    self.h = h
    self.players = {}
    self.next_player_id = 0

  def add_player(self, name, addr):
    x = random.randint(0, self.w)
    y = random.randint(0, self.h)
    new_player = Player(self.next_player_id, name, x, y, addr)
    self.players[self.next_player_id] = new_player
    self.next_player_id += 1
    return new_player

  def remove_player(self, addr):
    self.players = {k: v for k, v in self.players.iteritems() if v.addr != addr}

  def get_player_by_addr(self, addr):
    for player_id, player in self.players.iteritems():
      if player.addr == addr:
        return player
    return None

  def get_player_by_name(self, name):
    for player_id, player in self.players.iteritems():
      if player.name == name:
        return player
    return None

world_state = World(w=640, h=480)


class Message:
  def __init__(self, data, addr):
    self.data = data
    self.addr = addr


def send_message(msg, addr):
  client = clients[addr]
  msg['id'] = client.seq_num
  client.seq_num += 1

  # TODO: Think about it
  if msg['type'] == 'update':
    client.diffs[msg['id']] = msg['changes']

  packet = jsonpickle.encode(msg)
  print('[send/{addr}]: {msg}'.format(addr=addr, msg=packet))  # TODO: rm
  sock.sendto(packet, addr)


def broadcast_message(msg):
  for addr, _ in clients.iteritems():
    send_message(msg, addr)


def process_message(msg, addr):
  print('[recv/{addr}]: {msg}'.format(addr=addr, msg=msg))  # TODO: rm

  j = json.loads(msg)

  msg_type = j['type']
  if msg_type == 'ping':
    on_ping(addr)
  elif msg_type == 'connect':
    on_connect(j['name'], addr)
  elif msg_type == 'disconnect':
    on_disconnect(addr)
  elif msg_type == 'ack':
    on_ack(j['ack'], addr)
  elif msg_type == 'keypressed':
    on_keypressed(j['key'], addr)
  elif msg_type == 'keyreleased':
    on_keyreleased(j['key'], addr)
  else:
    print('Unknown message received: {msg}'.format(msg=msg))

  # send ack for reliable messages
  if j['reliable']:
    send_ack(j['id'], addr)


def on_ping(addr):
  send_message({
    'type': 'pong'
  }, addr)


def on_connect(name, addr):
  player = world_state.get_player_by_addr(addr)
  if player is None:
    if world_state.get_player_by_name(name) is None:
      player = world_state.add_player(name, addr)
    else:
      send_message({
        'type': 'connect',
        'success': False,
        'reason': 'Name is already in use'
      }, addr)
      return
  send_message({
    'type': 'connect',
    'success': True,
    'player': {
      'id': player.id
    }
  }, addr)


def on_disconnect(addr):
  world_state.remove_player(addr)


def on_ack(ack_id, addr):
  client = clients[addr]
  diff = client.diffs.get(ack_id)
  apply_state_diff(client.world_state, diff)
  del client.diffs[ack_id]


def on_keypressed(key, addr):
  for player_id, player in world_state.players.iteritems():
    if player.addr == addr:
      player.keys_states[key] = True
      break


def on_keyreleased(key, addr):
  for player_id, player in world_state.players.iteritems():
    if player.addr == addr:
      player.keys_states[key] = False
      break


def send_ack(id, addr):
  send_message({
    'type': 'ack',
    'ack': id
  }, addr)


def receive_messages():
  msgs = []

  while True:
    try:
      data, addr = sock.recvfrom(65565)
    except socket.error:
      break
    msgs.append(Message(data, addr))

  return msgs


def check_connections():
  cur_time = time.time()
  clients_copy = copy.deepcopy(clients)  # To avoid "RuntimeError: dictionary changed size during iteration"
  for addr, client in clients_copy.iteritems():
    if cur_time - client.last_msg_time > 5:
      del clients[addr]
      on_disconnect(addr)


def get_states_diff(player_state):
  diff = []

  for w_player_id, w_player in world_state.players.iteritems():
    p_player = player_state.get(w_player_id)
    if p_player is None or p_player != w_player:
      diff.append(w_player)

  for p_player_id, p_player in player_state.iteritems():
    if world_state.players.get(p_player_id) is None:
      remove_player = copy.deepcopy(p_player)
      remove_player.x = -1
      remove_player.y = -1
      diff.append(remove_player)

  return diff


def apply_state_diff(player_state, diff):
  for player in diff:
    if player.x == -1 or player.y == -1:
      del player_state[player.id]
    else:
      player_state[player.id] = copy.deepcopy(player)


def main():
  prev_time = time.time()
  dt = 0.0
  while True:
    # Process incoming messages
    msgs = receive_messages()
    for msg in msgs:
      # Check whether it's a new client
      if msg.addr not in clients:
        clients[msg.addr] = Client()
      # Touch it anyway to update last message time
      clients[msg.addr].touch()

      process_message(msg.data, msg.addr)

    # Check existing connections for timeouts
    check_connections()

    cur_time = time.time()
    dt += cur_time - prev_time
    prev_time = cur_time
    if dt > 0.03:
      # Apply physics
      for player_id, player in world_state.players.iteritems():
        speed = 50
        if player.keys_states['up']:
          print('up')
          player.y = player.y - speed * dt
        elif player.keys_states['down']:
          print('down')
          player.y = player.y + speed * dt
        elif player.keys_states['left']:
          print('left')
          player.x = player.x - speed * dt
        elif player.keys_states['right']:
          print('right')
          player.x = player.x + speed * dt

      # Send updates
      for addr, client in clients.iteritems():
        changes = get_states_diff(client.world_state)
        if len(changes) == 0:
          print('No changes')
          continue

        send_message({
          'type': 'update',
          'changes': changes
        }, addr)

      dt = 0.0


if __name__ == '__main__':
  main()

