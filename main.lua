local Gamestate = require 'lib.hump.gamestate'
local json = require 'lib.json.json'
local socket = require 'socket'

-- game states
local menu = {}
local load = {}
local game = {}

local font = love.graphics.newFont(14) -- default LOVE font
local playerName = ''

-- server info
local serverIP   = '127.0.0.1'
local serverPort = 12345

local udp = socket.udp()
udp:settimeout(0)
-- according to the documentation,
-- there is about 30% performance gain due to the call to setpeername method
udp:setpeername(serverIP, serverPort)

local sendQueue = {}
local seqNum = 0
local lastPingDt = 0.0
local timeout = 30.0
local lastServerMsgTime = 0.0

-------------------
-- menu
-------------------

function menu:enter(previous, err)
  -- enable key repeat so backspace can be held down to trigger love.keypressed multiple times
  love.keyboard.setKeyRepeat(true)

  self.err = err
end

function menu:draw()
  if self.err ~= nil then
    love.graphics.setColor({255, 0, 0})
    printCenterX(font, self.err, love.graphics:getHeight() / 2 - font:getHeight() * 2)
  end

  love.graphics.setColor({255, 255, 255})
  printCenterX(font, 'Type your name', love.graphics:getHeight() / 2 - font:getHeight())
  printCenterX(font, playerName .. '_', love.graphics:getHeight() / 2 + font:getHeight())
end

function menu:textinput(t)
  -- let's pretend that we're working with ASCII characters only
  -- TODO: Add UTF-8 support

  local maxPlayerNameLength = 16
  if string.len(playerName) < maxPlayerNameLength
     and font:hasGlyphs(t) then -- check whether the Font can render our characters
    playerName = playerName .. t
  end
end

function menu:keypressed(key)
  if key == 'backspace' then
    playerName = removeLast(playerName, 1) -- remove last character
  end
end

function menu:keyreleased(key, code)
  if key == 'return' then
    if string.len(playerName) > 0 then
      Gamestate.switch(load)
    end
  end
end

-------------------
-- load
-------------------

function load:enter()
  addMessageToQueue({
    type = 'connect',
    name = playerName
  }, true) -- true for reliable
end

function load:update()
  local msgs = receiveMessages()
  self:processMessages(msgs)
  sendMessages()
end

function load:draw()
  printCenterX(font, 'Connecting...', love.graphics:getHeight() / 2 - font:getHeight())
end

function load:processMessages(msgs)
  for i, msg in ipairs(msgs) do
    self:processMessage(msg)
  end
end

function load:processMessage(msg)
  print('[recv]: ' .. msg) -- TODO: rm

  local msg = json.decode(msg)

  if msg.type == 'connect' then
    -- TODO: Remove "connect" message from queue?
    self:handleConnect(msg)
  else
    print('[load] Unknown message type: ' .. msg.type)
  end
end

function load:handleConnect(msg)
  if msg.success then
    Gamestate.switch(game, msg.player.id)
  else
    Gamestate.switch(menu, msg.reason)
  end
end

-------------------
-- game
-------------------

function game:enter(previous, playerID)
  love.keyboard.setKeyRepeat(false)

  self.playerID = playerID
  self.players = {}
end

function game:update(dt)
  local msgs = receiveMessages()
  self:checkConnection()
  self:processMessages(msgs)

  self:handleInput(dt)

  self:handlePing(dt)
  sendMessages()
end

function game:draw()
  love.graphics.setColor({255, 0, 0})
  for addr, player in pairs(self.players) do
    love.graphics.rectangle('fill', player.x, player.y, 10, 10)
  end
end

function game:checkConnection()
  if lastServerMsgTime > timeout then
    self:onDisconnect()
  end
end

function game:onDisconnect()

end

function game:processMessages(msgs)
  for i, msg in ipairs(msgs) do
    self:processMessage(msg)
  end
end

function game:processMessage(msg)
  print('[recv]: ' .. msg) -- TODO: rm

  local msg = json.decode(msg)

  if msg.type == 'pong' then
    self:handlePong(msg)
  elseif msg.type == 'ack' then
    self:handleAck(msg)
  elseif msg.type == 'update' then
    self:handleUpdate(msg)
  else
    print('[game] Unknown message type: ' .. msg.type)
  end
end

function game:handlePong(msg)
  print('pong received') -- TODO: rm
end

function game:handleAck(msg)
  for i, sendMsg in ipairs(sendQueue) do
    if sendMsg.id == msg.ack then
      table.remove(sendQueue, i)
    end
  end
end

function game:handleUpdate(msg)
  for i, change in ipairs(msg.changes) do
    if change.x == -1 or change.y == -1 then
      self.players[change.id] = nil
    else
      self.players[change.id] = change
    end
  end

  addMessageToQueue({
    type = 'ack',
    ack = msg.id
  }, true) -- true for reliable
end

function game:handlePing(dt)
  lastPingDt = lastPingDt + dt
  if lastPingDt > 1.0 then
    addMessageToQueue({
      type = 'ping'
    })
    lastPingDt = 0.0
  end
end

function game:handleInput(dt)
  local speed = 50

  local player = self.players[self.playerID]

  if love.keyboard.isDown('up') then 
    player.y = player.y - speed * dt
  elseif love.keyboard.isDown('down') then 
    player.y = player.y + speed * dt
  elseif love.keyboard.isDown('left') then 
    player.x = player.x - speed * dt
  elseif love.keyboard.isDown('right') then
    player.x = player.x + speed * dt
  elseif love.keyboard.isDown('escape') then
    -- TODO: send "disconnect" message to the server
    love.event.push('quit')
  end
end

function game:keypressed(key)
  addMessageToQueue({
    type = 'keypressed',
    key = key
  }, true) -- true for reliable
end

function game:keyreleased(key)
  addMessageToQueue({
    type = 'keyreleased',
    key = key
  }, true) -- true for reliable
end

-------------------
-- network
-------------------

function receiveMessages()
  local msgs = {}

  while true do
    local msg = udp:receive()
    if msg == nil then
      break
    end

    lastServerMsgTime = socket.gettime()
    table.insert(msgs, msg)
  end

  return msgs
end

function sendMessages()
  for i, msg in ipairs(sendQueue) do
    udp:send(msg)
    if not msg.reliable then
      table.remove(sendQueue, i)
    end
  end
end

function addMessageToQueue(msg, reliable)
  msg.id = seqNum
  msg.reliable = reliable or false

  table.insert(sendQueue, json.encode(msg))

  seqNum = seqNum + 1
end

-------------------
-- helpers
-------------------

function printCenterX(font, text, y)
  local x = love.graphics:getWidth() / 2 - font:getWidth(text) / 2
  love.graphics.print(text, x, y)
end

function removeLast(s, n)
  return string.sub(s, 1, -1 - n)
end

-------------------
-- main
-------------------

function love.load()
  Gamestate.registerEvents()
  Gamestate.switch(menu)
end

